#include <Process.h>
int inPin[2] = {2,3};
int outPin[2] = {8,13};
int previous[2] = {LOW,LOW};
long time[2] = {0,0};
int state[2] ;
int reading[2];
int ini=1;
int state_ini;
long debounce = 200;
int web;
long previousMillis = 0;
long interval = 500;
int ini_pins=0;



void setup()
{
  Bridge.begin();
  for(int i=0;i<sizeof(outPin);i++){
    pinMode(outPin[i], OUTPUT);
  }
}

void loop()
{
  Process mysql;
  Process webstatus;
  Process webstatus_alter;
  reading[0] = digitalRead(inPin[0]);
  reading[1] = digitalRead(inPin[1]);
  unsigned long currentMillis = millis();
  
  if (ini==1){
    for(int i=1;i<=sizeof(inPin);i++){
      state[i-1] = func_status(i);
      if(state[i-1]<0){state[i-1]=0;}
      digitalWrite(outPin[i-1], state[i-1]);
    }
    ini=0;
  }
  
  for(int i=1;i<sizeof(inPin);i++){
    if (reading[i-1] == HIGH && previous[i-1] == LOW && millis() - time[i-1] > debounce) {
      if (state[i-1] == HIGH){
        state[i-1] = LOW;
        const String ledid=String(i);
        mysql.runShellCommand("sh ~/mysql.sh -1 "+ledid);
      }
      else{
        state[i-1] = HIGH;
        const String ledid=String(i);
        mysql.runShellCommand("sh ~/mysql.sh 1 "+ledid);
      }
      time[i-1] = millis();    
    }
    digitalWrite(outPin[i-1], state[i-1]);
    previous[i-1] = reading[i-1];
  }
  
  if(currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;    
    webstatus.runShellCommand("sh ~/webstatus.sh");
    while(webstatus.running());
    int web = webstatus.parseInt();
    if (web==1){
        ini=1;
        webstatus_alter.runShellCommand("sh ~/webstatus_alter.sh 0");
    }
  }
}

int func_status(int x){
    Process ledstatus;
    const String id=String(x);
    ledstatus.runShellCommand("sh ~/ledstatus.sh "+id);
    while(ledstatus.running());
    int led_status = ledstatus.parseInt();
    return led_status;
}
