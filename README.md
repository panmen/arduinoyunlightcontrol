# README

This is a proof of concept for setting up the newly released arduino Yun as light controller. A web server was setup on arduino Yun with support of MySQL and PHP. Light status is controlled with MySQL queries and even when a button is pressed triggers an SQL query, this is the advantage of Arduino Yun’s Bridge, you can interact between the ATmega 32U4 (Arduino side) and the Linino AR9331(Linux side). Below are some pictures of the setup and also a demo video.

[Youtube Video](https://www.youtube.com/watch?v=HcQAtPdrdyo)
